<?php

namespace Some\Big\Application\SomeFees;

class SomeRate
{
    private $r_value;
    private $fr_flag;
    private $tag;

    public function __construct($payload)
    {
        $this->r_value = $payload["r_value"];
        $this->fr_flag = $payload["fr_flag"];
        $this->tag = !empty($payload["tag"]) ? $payload["tag"] : "None";
    }

    public function __set($key, $value)
    {
        switch ($key) {
            case 'r_value':
                $this->r_value = $value;
                break;
            case 'fr_flag':
                $this->fr_flag = $value;
                break;
            case 'tag':
                $this->tag = $value;
                break;
            default:
        }
    }

    public function __get($key)
    {
        $returnable = null;

        switch ($key) {
            case 'r_value':
                $returnable = $this->r_value;
                break;
            case 'fr_flag':
                $returnable = $this->fr_flag;
                break;
            case 'tag':
                $returnable = $this->tag;
                break;
            default:
        }

        return $returnable;
    }
}
