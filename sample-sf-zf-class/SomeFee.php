<?php

namespace Some\Big\Application\SomeFees;

class SomeFee
{
    private $v_name;
    private $a_id;
    private $trigger;
    private $rates;

    public function __construct($payload)
    {
        $this->v_name = $payload["v_name"];
        $this->a_id = $payload["a_id"];
        $this->trigger = $payload["trigger"];
        $this->rates = [];
    }

    public function addRate(RoyaltyRate $rrv)
    {
        $this->rates[] = $rrv;
    }

    public function __set($key, $value)
    {
        switch ($key) {
            case 'v_name':
                $this->v_name = $value;
                break;
            case 'trigger':
                $this->trigger = $value;
                break;
            default:
        }
    }

    public function __get($key)
    {
        $returnable = null;

        switch ($key) {
            case 'v_name':
                $returnable = $this->v_name;
                break;
            case 'trigger':
                $returnable = $this->trigger;
                break;
            case 'a_id':
                $returnable = $this->a_id;
                break;
            case 'rates':
                $returnable = array_values($this->rates);
                break;
            default:
        }

        return $returnable;
    }
}
