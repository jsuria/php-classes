<?php

namespace Some\Big\Application\SomeFees;

class SomeList
{
    private $perks;

    public function __construct($payload = [])
    {
        $this->perks = $payload;
    }

    public function process()
    {
        if (!empty($this->perks)) {
            return array_map(
                function ($elem) {
                    $rf = new SomeFee([
                        "v_name" => $elem["v_name"],
                        "trigger" => "",
                        "a_id" =>  $elem["a_id"]
                    ]);

                    $rf->addRate(
                        new RoyaltyRate([
                            "r_value" => $elem["r_value"],
                            "fr_flag" => $elem["fr_flag"],
                            "tag" => "None"
                        ])
                    );

                    return $rf;
                },
                $this->perks
            );
        } else {
            return false;
        }
    }

    public function addToList(SomeFee $rf)
    {
        $this->perks[] = $rf;
    }
    
    public function getList()
    {
        return $this->perks;
    }
}
