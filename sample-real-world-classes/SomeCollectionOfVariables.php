<?php
/**
 * (Agency Admin) PortalValues Model
 *
 * PHP version 5
 *
 * @category  Model
 * @package   xxxx\xxxxxxx
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
namespace xxx\xxxxx;

class SomeCollectionOfVariables
{
    private $ws;

    private $agency_id;
    
    private $portal_values;

    // Aliases for long-winded array keys
    private $FLAG_KEYS_CONTAINER = 'admin_portal_flag_collection';

    private $FLAG_KEYS_VALUE_CONTAINER = 'value_collection';

    private $FLAG_KEYS_ID = 'flag_desc_id';

    private $AGENCY_KEYED_REPORTS_VALUES = array('P','B');

    // Group ID's (same across DEV, QA and PROD environments)
    private $GROUPID_CC_SELFSERVICE = '0';

    private $GROUPID_CC_ANALYTICS = '1';

    private $GROUPID_CC_IYETEK = '2';

    private $GROUPID_CC_DORS = '3';

    private $GROUPID_AGENCY_ACCT_SETUP = '5000';

    private $GROUPID_AGENCY_REDACT = '10000';

    private $GROUPID_AGENCY_MAP_SETTING = '15000';

    private $GROUPID_AGENCY_GUEST_USER_FLAG = '16000';

    private $_cvd_ws;

    private $_cvd_db_name = 'XXXXX';
    
    private $_cvd_tbl_name = 'agency';

    private $_cvd_col_name = 'agency_type';
            
    /**
     * Put all initializations hee-ah.
     */
    public function __construct(\XXXXX\WebService $ws, $agencyId, $preFetchFlags = true)
    {
        $this->ws = $ws;
        $this->agency_id = $agencyId;

        $this->portal_values = array();

        // CVD initialize
        $this->_cvd_ws = \XXXXXX\WebService::getInstance('WS_common');

        // Given how the controllers are structured, pre-fetching flags
        // is only necessary when you need to display them
        // No need to pre-fetch when instantiating to update flags
        if ($preFetchFlags) {
            $this->getFlagsFromWS();
        }
    }

    /**
     * Webservice handler
     * @return object
     */
    private function getWs()
    {
        return $this->ws;
    }

    /**
     * Utility wrapper method for returning WS results
     * @return array
     */
    private function getResult($array)
    {
        return array(
            'data' => $array[0],
            'message' => isset($array['message']) ? $array['message'] : null
        );
    }

    /**
     * Gets grouped Command Center items
     * @return array
     */
    public function getCommandCenterInfo()
    {
        return $this->portal_values['command_center'];
    }

    /**
     * Gets Agency Account Setup section items
     * @return array
     */
    public function getAgencyAccountSetupInfo()
    {
        return $this->getMappedData(
            $this->portal_values['non_command_center'][$this->GROUPID_AGENCY_ACCT_SETUP]
        );
    }

    /**
     * Gets agency types, for dropdown in agency account section
     * @param boolean
     * @return array
     */
    public function getAgencyTypes($includeMessage = false)
    {
        $result = $this->_cvd_ws->GetTableColumnValuesCollection(
            array(
              'database_name' => $this->_cvd_db_name,
              'column_value_entity_collection' => array(
                  'ColumnValueEntity' => array(
                      'database_name' => $this->_cvd_db_name,
                      'table_name' => $this->_cvd_tbl_name,
                      'column_name' => $this->_cvd_col_name
                  )
              )
            )
        );

        // Override array stored in 'data' key with list
        // Include message if flagged
        $tmp = $this->getResult($result);
        $data = $tmp['data']['agency_agency_type_display_list']['result'];
        
        return $includeMessage ? array(
            'data' => $data,
            'message' => $tmp['message']
        ) : $data;
    }

    /**
     * Gets Agency Redact settings
     * @return array
     */
    public function getAgencySettingRedactInfo()
    {
        return $this->getMappedData(
            $this->portal_values['non_command_center'][$this->GROUPID_AGENCY_REDACT]
        );
    }

    /**
     * Gets Agency Map info
     * @return array
     */
    public function getAgencyMapInfo()
    {
        return $this->getMappedData(
            $this->portal_values['non_command_center'][$this->GROUPID_AGENCY_MAP_SETTING]
        );
    }

    /**
     * Gets Allow Guest User Flag Info
     * @return array
     */
    public function getAllowGuestUserFlagInfo()
    {
        return $this->getMappedData(
            $this->portal_values['non_command_center'][$this->GROUPID_AGENCY_GUEST_USER_FLAG]
        );
    }

    /**
     * Gets Specified Agency Flag Value
     * @return array
     */
    public function submitsPaperReports()
    {
        $tmp =  $this->getAgencyAccountSetupInfo();
        return in_array(
            $tmp['agency_type']['value_collection']['result']['value'],
            $this->AGENCY_KEYED_REPORTS_VALUES
        );
    }
    
    /**
     * Wrapper function to group and sort items for display
     * @return array
     */
    private function processData($collection)
    {
        $groupWithDependencies = $this->getFlagDependency($collection);
        return $this->sortGroupItems($groupWithDependencies);
    }

    /**
     * Wrapper function to store each flag with flag_name as the key
     * @return array
     */
    private function getMappedData($sourceArray)
    {
        $new_payload = [];

        foreach ($sourceArray as $flag) {
            $new_payload[$flag['flag_name']] = $flag;
        }

        return $new_payload;
    }

    /**
     * Process result for dependencies
     * @return array
     */
    private function getFlagDependency($collection)
    {
        $processed = [];

        // First pass to do some item processing.
        foreach ($collection as $flag) {
            $flagId = $flag[$this->FLAG_KEYS_ID];
            $flagParentId = $flag['depends_on'];

            $processed[$flagId] = $flag;
            $processed[$flagId]['display'] = true;

            if ($flag['depends_on'] != '') {
                // If this item is dependent on another then hide if not set.
                $hasDependency = isset($processed[$flagParentId]);

                if ($hasDependency) {
                    $processed[$flagId]['display']  = $this->dependencyHasValue(
                        $processed,
                        $flagParentId
                    );
                    // Update parent field to know what depends on it.
                    $processed[$flagParentId]['dependent'] = $flag['flag_name'];
                }
            }
        }

        return $processed;
    }

    /**
     * Groups items by grouping id
     * @return array
     */
    private function sortGroupItems($collection)
    {
        $groups = array();

        // Second pass to group
        // Group using the grouping id attribute
        foreach ($collection as $item) {
            $grouping = $item['grouping'];

            if (!isset($groups[$grouping])) {
                $groups[$grouping] = array();
            }

            $groups[$grouping][] = $item;
        }

        // Sort so that group 0 comes first.
        ksort($groups);

        return $groups;
    }

    /**
     * Evaluates dependency for values
     * @return array
     */
    private function dependencyHasValue($collection, $key)
    {
        $container = $collection[$key][$this->FLAG_KEYS_VALUE_CONTAINER]['result']['value'];
        return (isset($container) && $container == '1');
    }

    /**
     * Gets agency portal values using flag
     * @return array
     */
    private function getPortalValues($agencyId, $isCommandCenter = false)
    {
        $result = $this->getWS()->GetAgencyAdminPortalValues(
            array(
                'agency_id'      => $agencyId,
                'is_command_ctr' => $isCommandCenter ? 1 : 0
            )
        );

        return $this->getResult($result);
    }

    /**
     * Gets portal values (command and non-command center)
     * @return array
     */
    public function getFlagsFromWS()
    {
        $resultCC = $this->getPortalValues($this->agency_id, true);
        $resultNonCC = $this->getPortalValues($this->agency_id, false);

        $this->portal_values['command_center'] = $this->processData($resultCC['data'][$this->FLAG_KEYS_CONTAINER]['flag']);
        $this->portal_values['non_command_center'] = $this->processData($resultNonCC['data'][$this->FLAG_KEYS_CONTAINER]['flag']);
    }

    /**
     * Set agency portal values
     * @return array
     */
    public function setAgencyPortalValues($payLoad)
    {
        $result = $this->getWS()->UpdateAdminPortalAgencyValues(
            array(
                'agency_id'      => $this->agency_id,
                'agencyValuesCollection' => $payLoad
            )
        );

        return $this->getResult($result);
    }
}
