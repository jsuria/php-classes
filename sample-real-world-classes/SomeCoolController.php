<?php

/**
 * SomeCoolController
 *
 * PHP version 5
 *
 * @category  Controller
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

class SomeCoolController extends BigApp\Controller
{
    
     private function assignAgencyPortalGroups($agencyId)
    {
        try {
            $portalValues = new xxx\xxx\xxx(
                $this->_wsXXXX,
                $agencyId
            );
            
            // Get value of agency type
            $this->view->AGENCY_SUBMITS_KEYED_REPORTS = $portalValues->submitsPaperReports();

            // Command Center info is in GROUPS
            $this->view->GROUPS = $portalValues->getCommandCenterInfo();

            // Agency Account Setup
            $this->view->GROUPS_AGENCY_ACCT_SETUP = $portalValues->getAgencyAccountSetupInfo();
            // Agency types
            $this->view->AGENCY_TYPES_LIST = $portalValues->getAgencyTypes();

            // Agency Redact Info
            $this->view->GROUPS_AGENCY_REDACT = $portalValues->getAgencySettingRedactInfo();

            // Agency Map Info
            $this->view->GROUPS_AGENCY_MAP_INFO = $portalValues->getAgencyMapInfo();

            // Agency Allow Guest User Flag Info
            $this->view->GROUPS_AGENCY_GUEST_USER_FLAG = $portalValues->getAllowGuestUserFlagInfo();
        } catch (\Exception $ex) {
            $response->fail($ex->getMessage());
        }
    }

     /**
     * Updates Admin Portal values
     *
     * @return void
     */
    public function updateAdminPortalAgencyValuesAction()
    {
        $this->checkPermission("ACCADMIN_EDITECRASHADMINPORTAL", true);
        $response = new XXX\JsonResponse();

        try {
            $this->_validation->addValidator('agency_id', $this->_validation->digitsRequired('Agency ID'));

            if (!$this->_validation->validate(null, $this->_getAllParams())) {
                $response->fail($this->_validation->getMessagesAsString());
            } else {
                // TODO: Use standard input, modify processCheckboxInput in the future
                // Uncomment once ready to use
                //$input = $this->_validation->getInput()->getEscaped();

                $params = $this->_getAllParams();

                $args = $this->processCheckboxInput($params);

                $sapv = new xxx\xxx\xxx(
                    $this->_wsxxx,
                    $params["agency_id"],
                    false
                );

                $results = $sapv->setAgencyPortalValues($args);

                if (!empty($results['message'])) {
                    $response->fail($results['message']);
                }
            }
        } catch (\Exception $ex) {
            $response->fail("Exception encountered: " . $ex->getMessage());
        }
        
        $this->sendJson($response);
    }
}