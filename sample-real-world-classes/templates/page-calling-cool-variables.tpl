<fieldset style="float: left; padding: 0px;margin: 4px 0px 4px 4px; width: 98.5%; height: 127px;">
    <legend>Account Set Up</legend>

    <table style="width: 99%;">
        <tbody>                            
            <tr>                              
                <td style="text-align: left;padding-left: 20px;">
                    Agency Type:
                </td>
                <td style="padding-right: 10px;">
                    <!--[agency_portal_fields 
                            source_variable=$GROUPS_AGENCY_ACCT_SETUP 
                            render_field="agency_type"
                            id="acct_setup_agency_type"
                            name="acct_setup_agency_type"
                            custom_value=$AGENCY_TYPES_LIST
                            custom_style="display: block;float: right;width: 80.5%;padding: 0px;margin: 0px;"
                            ]-->
                </td>
                    
            </tr>

            <tr id="row_agency_acct_hold" <!--[if !$AGENCY_SUBMITS_KEYED_REPORTS]--> style="display:none" <!--[/if]-->>                              
                <td style="text-align: left;padding-left: 20px;">
                    Hold for Batch Prep:
                </td>
                <td style="padding-left: 42px;">
                    <!--[agency_portal_fields 
                            source_variable=$GROUPS_AGENCY_ACCT_SETUP
                            render_field="hold_for_batch_prep"                            
                            id="acct_setup_hold_batch"
                            name="acct_setup_hold_batch"
                            custom_style="float: left;"
                            ]-->
                </td>
            </tr>
 
            <tr id="row_agency_acct_ftp" <!--[if !$AGENCY_SUBMITS_KEYED_REPORTS]--> style="display:none" <!--[/if]-->>                              
                <td style="text-align: left;padding-left: 20px;">
                    Incoming FTP folder:
                </td>
                <td style="padding-right: 10px;">
                    <!--[agency_portal_fields 
                            source_variable=$GROUPS_AGENCY_ACCT_SETUP 
                            render_field="incoming_ftp_folder"
                            id="acct_setup_ftp_folder"
                            name="acct_setup_ftp_folder"
                            custom_style="width: 79.5%;padding: 0px;float: right;margin: 0px;"
                            ]-->
                </td>
            </tr>
             
            <tr>                              
                <td style="text-align: left;padding-left: 20px;vertical-align: top;">
                    Notes:
                </td>
                <td style="vertical-align: top;padding-right: 10px;">
                    <!--[agency_portal_fields 
                            source_variable=$GROUPS_AGENCY_ACCT_SETUP 
                            render_field="notes_text"
                            id="acct_setup_notes_text"
                            name="acct_setup_notes_text"
                            custom_style="display: block;float: right;width: 80%;height: 50px;margin: 0px;padding: 0px; resize:none"
                            ]-->                            
                </td>
            </tr>
           
        </tbody>

    </table>
</fieldset>