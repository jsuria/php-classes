<?php
/**
 * Smarty modifier to format search data for eCrash Order Activity Report
 *
 * @param string $cell
 * @return string
 */
function smarty_function_otp_phonetypes_list($params)
{
    $source = $params['source_variable'];
    $default_value = $params['default_value'];
    $id = $params['id'];
    $name = $params['name'];

    // Flag whether to show as a list or not
    $show_as_list = $params['show_as_list'];

    if ($show_as_list) {
        $html = '<select name="' . $name . '" id="' . $id . '" style="margin-left: 0px !important;">';

        foreach ($source as $phone_type) {
            $description =  str_replace('Type:', '', $phone_type['description']);
            $clean_description = $description == 'text' ? 'Text (SMS)' : 'Voice';

            $value =  $phone_type['value'];

            $html .= '<option value="' . $value . '" ' . ($default_value == $value ? 'selected' : '') . '>'
                  . $clean_description
                  . '</option>';
        }

        $html .= '</select>';
    } else {
        // For display only (show the current value)
        $description =  str_replace('Type:', '', $default_value);
        $clean_description = $description == 'text' ? 'Text (SMS)' : 'Voice';

        $html = '<input type="text" value="'
                . $clean_description
                . '" style="margin:0px; width: 65%" disabled/>';
    }

    return $html;
}
