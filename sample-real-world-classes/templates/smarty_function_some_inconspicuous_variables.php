<?php

/**
 * This function displays a state input widget, which
 * may be a list of US states if the country is US,
 * otherwise it will display a text input box
 *
 * @param array $params
 */
function smarty_function_agency_portal_fields($params)
{
    try {
        $field = $params['render_field'];
        $field_source = $params['source_variable'];
        $field_custom = !empty($params['custom_value']) ? $params['custom_value'] : '';
        $field_css = !empty($params['custom_style']) ? $params['custom_style'] : '';

        // Put field types that require custom values
        $types_with_custom_values = array(
            'agency_type',
            'map_zoom_level'
        );

        // Collate the changes needed into a string
        $final_string = [];

        $flag_values = $field_source[$field];

        $default_identifier = $flag_values['flag_name'] . '_' . $flag_values['flag_desc_id'] . '_' . $flag_values['on_agency_tbl'];

        $id = !empty($params['id']) ? $params['id'] : $default_identifier;
        $name = !empty($params['name']) ? $params['name'] : $default_identifier;

        // Flag identifiers
        $input_hidden_id = 'hidden_' . $flag_values['flag_name'];
        $input_hidden_val = $flag_values['flag_desc_id'] . '_' . $flag_values['on_agency_tbl'];

        $value = in_array($field, $types_with_custom_values) ? $field_custom : $flag_values['value_collection']['result'];
        //$value = $flag_values['value_collection']['result'];
        $control_type = $flag_values['control_type'];

        // Flag this field as default (used for radio buttons)
        $field_as_default = !empty($params['is_default']) ? ((bool)$params['is_default']) : false;

        switch ($control_type) {
            case 'DropDown':
                $html = '<select style="display: block;float: right;width: 80%;" name="' . $name . '" id="' . $id . '">';

                if ($field == 'agency_type') {
                    // Get selected value
                    $agency_value = $flag_values['value_collection']['result']['value'];

                    foreach ($field_custom as $option) {
                        $selected = $option['value'] == $agency_value ? 'selected' : '';
                        $html .= '<option value="' . $option['value'] . '" ' . $selected . '>' . $option['description'] . '</option>';
                    }
                } else {
                    foreach ($value as $option) {
                        $html .= '<option value="' . $option['value'] . '">' . $option['value'] . '</option>';
                    }
                }
                
                $html .= '</select>';
                //  Additional fields for flag_desc_id and on_agency_tbl
                $html .= '<input type="hidden" name="' . $input_hidden_id . '" id="' . $input_hidden_id . '" value="' . $input_hidden_val. '"/>';

                $final_string[] = $html;

                break;
            case 'CheckBox':
                $checked = ($value['value'] == 'on' || $value['value'] == '1') ? 'checked="checked"' : '';
                $html = '<input type="checkbox" '. $checked . ' name="' . $name . '" id="' . $id . '" style="' . $field_css . '"/>';

                //  Additional fields for flag_desc_id and on_agency_tbl
                $html .= '<input type="hidden" name="' . $input_hidden_id . '" id="' . $input_hidden_id . '" value="' . $input_hidden_val. '"/>';

                $final_string[] = $html;

                break;
            case 'RadioButton':
                /**/
                if ($flag_values['flag_name'] == 'map_zoom_level') {
                    // Compare custom value
                    if ($value == $flag_values['value_collection']['result']['value']) {
                        $checked = 'checked="checked"';
                    } else {
                        $checked = '';
                    }
                } else {
                    // Other scenarios might present themselves
                }
                
                $html = '<input type="radio" '
                        . ' name="' . $name . '" id="' . $id. '" style="' . $field_css . '" value="' . $value . '"'
                        . $checked . '" />';

                //  Additional fields for flag_desc_id and on_agency_tbl
                $html .= '<input type="hidden" name="' . $input_hidden_id . '" id="' . $input_hidden_id . '" value="' . $input_hidden_val. '"/>';

                $final_string[] = $html;

                break;
            case 'TextBox':
                /**/
                if ($flag_values['flag_name'] == 'notes_text') {
                    $maxlength = 'maxlength=200';

                    $html = '<textarea ' . $maxlength . ' style="' . $field_css . '" name="' . $name . '" id="' . $id . '">' . $value['value'] . '</textarea>';
                } else {
                    $maxlength = "";

                    $html = '<input type="text" value="'. $value['value']
                          . '" name="' . $name
                          . '" id="' . $id . '" '. $maxlength
                          .' style="' . $field_css . '"/>';
                }

                //  Additional fields for flag_desc_id and on_agency_tbl
                $html .= '<input type="hidden" name="' . $input_hidden_id . '" id="' . $input_hidden_id . '" value="' . $input_hidden_val. '"/>';

                $final_string[] = $html;
                
                break;
        }

        return implode('', $final_string);
    } catch (\Exception $ex) {
        return $ex->getMessage();
    }
}
