<div style="margin-bottom: 50px">
                    
    <fieldset>
        <legend>One Time Passcode (OTP) Options</legend>

        <div style="text-align: center;">
             <!--[otp_table_list 
                    source_variable=$BUYCRASHOTP_INFO 
                    css_class="sortable rowstyle-alt colstyle-alt onload-zebra"
                    custom_css="width: 80%"
                    ]-->
        </div>    
    </fieldset>

</div>

<script type="text/javascript">
    YAHOO.util.Event.onDOMReady(function() {

        var activeTab = 'div#accadmin_user_detail_tabs_div li.selected';

        updateBuyCrashOTPInfo = function(mode) {

            /**/
            // Added email validation on the client side
            // TODO: Find a better implementation. 
            // Zend email validation API even allows non-standard but LEGAL email address formats.
            var validateForm = function(){
                var msgError = "Invalid email format. Please check and try again.";
                var val = $(this.form).find('input#otp_email').val();
                
                // Edit mode is for email
                if(mode == '0'){
                    if ( /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val) )  
                    {  
                        return true;
                    } else {
                        YAHOO.mbs.showSimpleDialog(msgError, "Invalid input", YAHOO.widget.SimpleDialog.ICON_BLOCK);
                        return false;    
                    }
                // Bypass for phone validation (done on backend)
                } else {
                    return true;
                }
            };

            var titles =  ['Email', 'Phone 1', 'Phone 2'];

            YAHOO.mbs.showFormDialog(
                '/accadmin/user/show-buycrash-otp-form/c/' + <!--[$COMPANY_ID]--> + '/u/' + <!--[$USER_ID]--> + '/m/' + mode,
                /* Config */
                {
                    width: '600px',
                    height: '140px',
                    title: 'Update OTP ' + titles[mode],
                    fixedcenter: true,
                    buttons: [                        
                        { 
                            text: 'Cancel',
                            handler: function() {
                                this.hide();
                            }                            
                        },
                        {
                            text: 'Save',
                            handler: function() { 
                                this.submit(); 
                            },
                            isDefault: true
                        }
                    ]
                },
                /* Callback */
                {
                    success: function(data) {

                        var response = YAHOO.lang.JSON.parse(data.responseText);

                        if (response.status == 'success' || response.status == 200) {
                            YAHOO.mbs.showSimpleDialog(
                                'OTP info updated!', 
                                "Success", 
                                YAHOO.widget.SimpleDialog.ICON_INFO
                            );

                            this.hide();
                            $(activeTab).trigger('click');
                            
                        } else {
                            YAHOO.mbs.showSimpleDialog(
                                response.message, 
                                "Failed", 
                                YAHOO.widget.SimpleDialog.ICON_WARN
                            );
                            return;
                        }                    
                    }
                },
                /* Validation */
                validateForm
            );
            
            return false;
        };
      
    });
</script>