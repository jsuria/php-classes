<?php
/**
 * Generates table of OTP phone numbers
 *
 * @param string
 * @return string
 */
function smarty_function_otp_table_list($params)
{
    $source = $params['source_variable'];
    $class = $params['css_class'];
    $css = $params['custom_css'];

    $tmp_email = explode(' ', $source['email']['display_name']);
    $clean_email_display = strtoupper($tmp_email[1]) . ' ' . ucfirst($tmp_email[2]);

    $html = '<table class="' .  $class . '" style="' . $css . '">'
                . '<thead>'
                . '<tr>'
                    . '<th>OTP Channel</th>'
                    . '<th>Value</th>'
                    . '<th>Type</th>'
                    . '<th>Action</th>'
                . '</tr>'
                . '</thead>'
                . '<tbody>'
                . '<tr>'
                    . '<td>' . $clean_email_display . '</td>'
                    . '<td>' . $source['email']['value'] . '</td>'
                        . '<td>&nbsp;</td>'
                        . '<td><a href="#" id="update-buycrash-otp-email" onClick="javascript:updateBuyCrashOTPInfo(0);">Edit</a></td>'
                    .'</tr>';

    //foreach ($source['phones'] as $phone) {
    foreach ($source['phones'] as $key => $phone) {
        $tmp_phone =  explode(' ', $phone['value']['display_name']);
        $clean_phone_display = strtoupper($tmp_phone[1]) . ' ' . ucfirst($tmp_phone[2]) . ' ' . $tmp_phone[3];

        $value =  $phone['value']['value'];
        $tmp =  str_replace('Type:', '', $phone['type']['value']);
        $type = $tmp == 'text' ? 'Text (SMS)' : 'Voice';

        $html .= '<tr>'
                   . '<td>' . $clean_phone_display . '</td>'
                   . '<td>' . $value . '</td>'
                   . '<td>' . ucfirst($type) . '</td>'
                   . '<td><a href="#" id="update-buycrash-otp-phone" onClick="javascript:updateBuyCrashOTPInfo(' . $key . ');">Edit</a></td>'
                .'</tr>';
    }
    return $html;
}
