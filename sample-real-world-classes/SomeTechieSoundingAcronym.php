<?php
/**
 * SomeTechieSoundingAcronym Model
 *
 * PHP version 5
 *
 * @category  Model
 * @package   xxx\xxx
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
namespace xxx\xxx\xxx;

use xxx\Validation;

class SomeTechieSoundingAcronym extends SomeGenericCommercialClass
{
    private $_company_id;
    private $_user_id;
    private $_category;
    private $_sub_categories;
    private $_phone_attributes;
    private $_phone_types;
    private $_email_attributes;
    private $_cvd_ws;
    private $_cvd_tbl_name;
    private $_cvd_col_name;

    /**
     * Initializations, implicitly call parent since we're overriding main constructor
     */
    public function __construct(\xxx\WebService $ws, $company_id, $user_id)
    {
        parent::__construct($ws);

        $this->_company_id = $company_id;
        $this->_user_id = $user_id;
        $this->_category = "User Options";
        $this->_sub_categories = array(
                'email'  => 'xxx otp email',
                'phone1' => 'xxx otp phone 1',
                'phone2' => 'xxx otp phone 2'
            );

        /****************************************/
        /* Modify as needed, more may be added */
        /****************************************/

        $this->_phone_types = array(
                'Type:voice'  => 'Voice',
                'Type:text'   => 'SMS'
            );
        // Type of data, used for render_type parameter
        $this->_phone_attributes = array(
                'value' => '1',
                'type' => '2',
            );
        // As assigned by WS
        $this->_email_attributes = array(
                'default' => '2'
            );

        // CVD parameters
        $this->_cvd_ws = \xxx\WebService::getInstance('WS_xxxxx');

        $this->_cvd_tbl_name = 'xxxxx_admin';
        $this->_cvd_col_name = 'User Options-xxxxx otp phone 1-2';
    }

    /**
     * Gets phone types
     * @return array
     *
     */
    public function getTypeOptions()
    {
        $result = $this->_cvd_ws->GetColumnValueDescription(
            array(
                'table_name' => $this->_cvd_tbl_name,
                'column_name' => $this->_cvd_col_name
            )
        );

        return $result;
    }
    
    /**
     * Returns the OTP email
     * @return array
     */
    public function getEmailInfo()
    {
        $result =  $this->getWS()->GetxxxxxxxTagByCategorySubCategory(
            array(
                'company_id' => $this->_company_id,
                'user_id' => $this->_user_id,
                'category' => $this->_category,
                'sub_category' => $this->_sub_categories['email'],
                'render_type' => $this->_email_attributes['default']
            )
        );

        return $this->getResult($result);
    }

    /**
     * Returns the specified phone info (label and attribute)
     * @return array
     */
    public function getPhoneInfo($params)
    {
        $postFix = $params['phone_postfix'];
        $attribute = $params['phone_attribute'];

        $result =  $this->getPhoneAttribute(
            array(
                'phone' => $this->_sub_categories['phone' . $postFix],
                'attribute' => $this->_phone_attributes[$attribute]
            )
        );

        return $this->getResult($result);
    }

    /**
     * Updates OTP email
     * @return void
     */
    public function setEmail($params)
    {
        $validation = new Validation();
        $validation->addValidator('otp_email', $validation->emailaddress('Email Address'));
        $validation->assert('<br>', null, $params);
        $request = $validation->getInput();

        // Precaution in case $request->otp_email
        // (happens when $validation->getInput()->getEscaped() is used)
        if (!empty($request->otp_email)) {
            $result = $this->getWS()->GetxxxxxxxTagByCategorySubCategory(
                array(
                    'company_id' => $this->_company_id,
                    'user_id' => $this->_user_id,
                    'category' => $this->_category,
                    'sub_category' => $this->_sub_categories['email'],
                    'render_type' => $this->_email_attributes['default'],
                    'value' => $request->otp_email
                )
            );
        } else {
            $result['message'] = "Notice: Email value not provided.";
        }

        return $this->getResult($result);
    }

    /**
     * Updates OTP phone
     * @return void
     */
    public function setPhone($params)
    {
        $validation = new Validation();

        $validation->addValidator('otp_phone1_value', $validation->phonenumber('OTP Phone 1'));
        $validation->addValidator('otp_phone1_type', $validation->inArray(
            'OTP Phone 1 Type',
            array_keys(
                $this->_phone_types
            )
        ));
        $validation->addValidator('otp_phone2_value', $validation->phonenumber('OTP Phone 2'));
        $validation->addValidator('otp_phone2_type', $validation->inArray(
            'OTP Phone 2 Type',
            array_keys(
                $this->_phone_types
            )
        ));
        $validation->assert('<br>', null, $params);
        $request = $validation->getInput();

        // Phone 1
        /************************************************************************/
        // Re-adding the checking for empty values
        if (!empty($request->otp_phone1_value)) {
            $results['phone1']['value'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone1'] ,     // Phone 1
                    'attribute' => $this->_phone_attributes['value'], // Value attribute
                    'attribute_value' => $request->otp_phone1_value   // Value
                )
            );
            
            // Stack messages if encountered
            if (!empty($results['phone1']['value']['message'])) {
                $results['messages'][] = $results['phone1']['value']['message'];
            }

            $results['phone1']['type'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone1'],      // Phone 1
                    'attribute' => $this->_phone_attributes['type'],  // Type attribute
                    'attribute_value' => $request->otp_phone1_type    // Value
                )
            );
            if (!empty($results['phone1']['type']['message'])) {
                $results['messages'][] = $results['phone1']['type']['message'];
            }
        }

        // Phone 2
        /************************************************************************/
        // Re-adding the checking for empty values
        if (!empty($request->otp_phone2_value)) {
            $results['phone2']['value'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone2'],          // Phone 1
                    'attribute' => $this->_phone_attributes['value'],     // Value attribute
                    'attribute_value' => $request->otp_phone2_value       // Value
                )
            );
            if (!empty($results['phone2']['value']['message'])) {
                $results['messages'][] = $results['phone2']['value']['message'];
            }

            $results['phone2']['type'] = $this->setPhoneAttribute(
                array(
                    'phone' => $this->_sub_categories['phone2'],          // Phone 1
                    'attribute' => $this->_phone_attributes['type'],      // Value attribute
                    'attribute_value' => $request->otp_phone2_type        // Value
                )
            );
            if (!empty($results['phone2']['type']['message'])) {
                $results['messages'][] = $results['phone2']['type']['message'];
            }
        }

        return $results;
    }

    /**
     * Get OTP Phone attribute (type or number)
     * @return array $params Params
     * @return array
     */
    private function getPhoneAttribute($params)
    {
        return $this->getWS()->GetxxxxxxxTagByCategorySubCategory(
            array(
                'company_id' => $this->_company_id,
                'user_id' => $this->_user_id,
                'category' => $this->_category,
                'sub_category' => $params['phone'],
                'render_type' => $params['attribute']
            )
        );
    }

    /**
     * Updates OTP Phone attribute (type or number)
     * @return array $params Params
     * @return array
     */
    private function setPhoneAttribute($params)
    {
        $result =  $this->getWS()->SetxxxxxxxTagByCategorySubCategory(
            array(
                'company_id' => $this->_company_id,
                'user_id' => $this->_user_id,
                'category' => $this->_category,
                'sub_category' => $params['phone'],
                'render_type' => $params['attribute'],
                'value' => $params['attribute_value']
            )
        );

        $returnable = $this->getResult($result);
    }

    /**
     * Processes phone types to use custom names
     * Added as fail-safe in case WS values revert
     * @return array
     *
     */
    private function useCustomPhoneTypes($payloadDuJour, $useValueAsDescription)
    {
        for ($i=0; $i < sizeof($payloadDuJour); $i++) {
            $new_value = $this->_phone_types[$payloadDuJour[$i]['value']];
            $payloadDuJour[$i]['value'] = $new_value;

            if ($useValueAsDescription) {
                $payloadDuJour[$i]['description'] = $new_value;
            }
        }

        return $payloadDuJour;
    }
}
