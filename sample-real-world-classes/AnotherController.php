<?php

/**
 * AnotherController
 *
 * PHP version 5
 *
 * @category  Controller
 * @package   xxx\xxx
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
 
use BigApp\Core\Factory as SomeFactory;

class AnotherController
{
    /**
     * Gets all OTP Info
     * @return array
     */
    private function getOTPInfo($companyId, $userId)
    {
        try {
            $buycrashotp = new \xxx\xxxx\xxx\xxx(
                $this->_wsAccurint,
                $companyId,
                $userId
            );

            $emailInfo = $buycrashotp->getEmailInfo();
            $otpInfo['email'] = $emailInfo['data'];

            $otpInfo['phones']['1'] = $this->getBuyCrashOTPPhone('1', $companyId, $userId);
            $otpInfo['phones']['2'] = $this->getBuyCrashOTPPhone('2', $companyId, $userId);
            
            return $otpInfo;
        } catch (\Exception $e) {
            $this->view->BUYCRASHOTP_MESSAGE = $e->getMessage();
        }
    }

    /**
     * Display BuyCrashOTP Update Form
     * @return void
     */
    public function showBuycrashOtpFormAction()
    {
        $editFlag  = $this->_getParam('m');
        $companyId = $this->_getParam('c');
        $userId    = $this->_getParam('u');

        $buycrashotp = new \xxx\xxxx\xxx\xxx(
            $this->_wsAccurint,
            $companyId,
            $userId
        );

        //$mode = ['email','phone'];
        $mode = ['email','phone1', 'phone2'];

        $this->view->BUYCRASHOTP_FORM_PHONE_TYPES = $buycrashotp->getTypeOptions();
        $this->view->COMPANY_ID = $companyId;
        $this->view->USER_ID = $userId;

        $this->view->BUYCRASHOTP_FORM_EDIT_MODE = $editFlag;
        
        switch ($mode[$editFlag]) {
            case 'email':
                $emailInfo = $buycrashotp->getEmailInfo();
                $this->view->BUYCRASHOTP_FORM_EMAIL = $emailInfo['data']['value'];
                break;
            case 'phone1':
                $phoneInfo['1'] = $this->getBuyCrashOTPPhone('1', $companyId, $userId);
                //$phoneInfo['2'] = $this->getBuyCrashOTPPhone('2', $companyId, $userId);
                $this->view->BUYCRASHOTP_FORM_PHONES = $phoneInfo;
                break;
            case 'phone2':
                $phoneInfo['2'] = $this->getBuyCrashOTPPhone('2', $companyId, $userId);
                $this->view->BUYCRASHOTP_FORM_PHONES = $phoneInfo;
                break;
            default:
                // Any default behaviour, if ever...
        }
    }
}

?>